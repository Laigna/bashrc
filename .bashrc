alias grep='grep --color --ignore-case'       # show differences in colour
alias egrep='egrep --color=auto'              # show differences in colour
alias fgrep='fgrep --color=auto'              # show differences in colour

# Some shortcuts for different directory listings
alias ls='ls -hF --color=tty'                 # classify files in colour
alias dir='ls --color=auto --format=vertical'
alias vdir='ls --color=auto --format=long'
alias ll='ls -l'                              # long list
alias la='ls -A'                              # all but . and ..
alias l='ls -CF'                              #

# Clears the screen for real.
alias cls='printf '\''\033c'\'''

# Docker related stuff
export COMPOSE_CONVERT_WINDOWS_PATHS=1 # Enable path conversion from Windows-style to Unix-style in volume definitions.
alias d='docker'
alias dc='d container'
alias dps='d ps --no-trunc'
alias ds='d service'
alias dsls='ds ls'
alias dsps='ds ps --no-trunc'
alias dreset='ds update --force'
alias dscale='d service scale'
alias dstack='d stack'
alias dstats='d stats --no-stream'
did() { d ps -q -f name="$1"; } # Get the container id.
dlogs() { d logs $(didof "$1"); } # Get the logs from some container.
dlogsf() { d logs -f --tail 20 $(didof "$1"); }
dexec() { dc exec $(didof "$1") "${@:2}"; }
dbash() { dc exec -it $(didof "$1") /bin/bash; }
dci() { dc inspect $(didof "$1"); }

# Git stuff
alias g='git'
alias gstatus='g status -sb'
alias gdiff='g diff'
alias gstaged='g diff --staged'

if [[ $(uname -s) == MINGW64* ]]
then 
  echo "Running on MINGW64"
  export MSYS_NO_PATHCONV=1 # Don't convert / in the begging of a path to git bin path (MINGW64 quirk).
  dbash() { winpty docker exec -it $(didof "$1") //bin/bash; }
fi
